﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Promptkart.Web.Startup))]
namespace Promptkart.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
