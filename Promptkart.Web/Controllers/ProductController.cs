﻿using Promptkart.Entities;
using Promptkart.Services;
using Promptkart.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Promptkart.Web.Controllers
{
    public class ProductController : Controller
    {
        ProductsService productsService;
        CategoriesService categoriesService;
        public ProductController()
        {
            productsService = new ProductsService();
            categoriesService = new CategoriesService();
        }
        // GET: Product
        public ActionResult Index()
        {
           List<Product>  lstProducts= productsService.GetProducts();
            return View(lstProducts);
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            List<Category> categories = categoriesService.GetCategories();

            return PartialView(categories);
        }

        [HttpPost]
        public ActionResult AddProduct(CategoryViewModel cvm)
        {
            Product newProduct = new Product();
            newProduct.Name = cvm.Name;
            newProduct.Description = cvm.Description;
            newProduct.Price = cvm.Price;
            newProduct.Category = categoriesService.GetCategory(cvm.CategoryID);

            productsService.SaveProduct(newProduct);

            return RedirectToAction("ProductTable");
        }
        [HttpGet]
        public ActionResult UpdateProduct(int id)
        {
            Product product = productsService.GetProduct(id);

            return PartialView(product);
        }

        [HttpPost]
        public ActionResult UpdateProduct(Product product)
        {
            productsService.EditProduct(product);
            return RedirectToAction("ProductTable");
        }

        [HttpGet]
        public ActionResult DeleteProduct(int id)
        {
           Product product= productsService.GetProduct(id);
            return View(product);
        }

        [HttpPost]
        [ActionName("DeleteProduct")]
        public ActionResult DeleteProduct_Post(int id)
        {
            productsService.DeleteProduct(id);
            return RedirectToAction("ProductTable");
        }

        public ActionResult ProductTable(string strSearch)
        {
            List<Product> lstProducts = productsService.GetProducts();

            if (!string.IsNullOrEmpty(strSearch))
            {
                 lstProducts = lstProducts.Where(p => p.Name !=null && p.Name.ToLower().Contains(strSearch.ToLower())).ToList();
            }
            return  PartialView(lstProducts);
        }

    }
}