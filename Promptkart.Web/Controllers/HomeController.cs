﻿using Promptkart.Services;
using Promptkart.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Promptkart.Web.Controllers
{
    public class HomeController : Controller
    {
        CategoriesService cs = new CategoriesService();

        public ActionResult Index()
        {
            HomeViewModel hvm = new HomeViewModel();

            hvm.categories = cs.GetCategories();

            return View(hvm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}