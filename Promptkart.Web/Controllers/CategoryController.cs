﻿using Promptkart.Entities;
using Promptkart.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Promptkart.Web.Controllers
{
    public class CategoryController : Controller
    {
        CategoriesService categoriesService;
        public CategoryController()
        {
            categoriesService = new CategoriesService();
        }

        // GET: Category
        public ActionResult Index()
        {
            List<Category> categories= categoriesService.GetCategories();
            return View(categories);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoriesService.SaveCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult EditCategory(int id)
        {
           Category category= categoriesService.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public ActionResult EditCategory(Category category)
        {
            categoriesService.EditCategory(category);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteCategory(int id)
        {
            Category category = categoriesService.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        [ActionName("DeleteCategory")]
        public ActionResult DeleteCategory_Post(int id)
        {
            categoriesService.DeleteCategory(id);
            return RedirectToAction("Index");
        }

    }
}