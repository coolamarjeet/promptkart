﻿using Promptkart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promptkart.Web.ViewModels
{
    public class HomeViewModel
    {
        public List<Category> categories { get; set; }
    }
}