﻿using Promptkart.Database;
using Promptkart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Promptkart.Services
{
    public class ProductsService
    {
        public void SaveProduct(Product product)
        {
            using (PKContext pk = new PKContext())
            {
                pk.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;

                pk.Products.Add(product);
                pk.SaveChanges();
            }
        }
        public List<Product> GetProducts()
        {
            //PKContext pk = new PKContext();
            //return pk.Products.ToList();
            using (PKContext pk = new PKContext())
            {
                return pk.Products.Include(x=>x.Category).ToList();
            }
        }
        public Product GetProduct(int id)
        {
            using (PKContext pk = new PKContext())
            {
                Product product = pk.Products.Find(id);
                return product;
            }
        }
        public void EditProduct(Product product)
        {
            using (PKContext pk = new PKContext())
            {
                pk.Entry(product).State = System.Data.Entity.EntityState.Modified;
                pk.SaveChanges();
            }
        }
        public void DeleteProduct(int id)
        {
            using (PKContext pk = new PKContext())
            {
                Product product = pk.Products.Find(id);
                pk.Products.Remove(product);
                pk.SaveChanges();
            }
     
        }

    }
}
