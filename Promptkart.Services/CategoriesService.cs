﻿using Promptkart.Database;
using Promptkart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promptkart.Services
{
    public class CategoriesService
    {
        public void SaveCategory(Category category)
        {
            using (var pkContext = new PKContext())
            {
                pkContext.Categories.Add(category);
                pkContext.SaveChanges();
            }
        }

        public List<Category> GetCategories()
        {
            using (var pkContext = new PKContext())
            {
                List<Category> lstCategories = pkContext.Categories.ToList();
                return lstCategories;
            }
        }

        public Category GetCategory(int ID)
        {
            using (var pkContext = new PKContext())
            {
                Category category = pkContext.Categories.Find(ID);
                return category;
            }
        }

        public void EditCategory(Category category)
        {
            using (var pkContext = new PKContext())
            {
                pkContext.Entry(category).State = System.Data.Entity.EntityState.Modified;
                pkContext.SaveChanges();

            }

        }

        public void DeleteCategory(int id)
        {
            using (var pkContext = new PKContext())
            {
                Category category = pkContext.Categories.Find(id);

                //   pkContext.Entry(category).State = System.Data.Entity.EntityState.Deleted;

                pkContext.Categories.Remove(category);
                pkContext.SaveChanges();

            }
        }

    }
}
