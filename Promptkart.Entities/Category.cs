﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promptkart.Entities
{
    public class Category:BaseEnitity
    {
        public string ImageURL { get; set; }
        public List<Product> Products { get; set; }
    }
}
