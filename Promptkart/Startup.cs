﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Promptkart.Startup))]
namespace Promptkart
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
